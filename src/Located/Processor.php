<?php

declare(strict_types=1);

namespace App\Located;


interface Processor
{

    public function process($thing);

}
