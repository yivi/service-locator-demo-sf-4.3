<?php

declare(strict_types=1);

namespace App\Located;

class Bar implements Processor
{

    public static function indexMe(): string
    {
        return 'barFile';
    }

    public function process($thing)
    {
        return 'Welcome to Bar. We received: ' . $thing;
    }

}
