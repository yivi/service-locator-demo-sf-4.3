<?php

declare(strict_types=1);

namespace App\Located;

class Foo implements Processor
{

    public static function indexMe(): string
    {
        return 'fooFile';
    }

    public function process($thing)
    {
        return 'This is the land of Foo, you gave us:  ' . $thing;
    }

}
