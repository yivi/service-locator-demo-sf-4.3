<?php

declare(strict_types=1);

namespace App\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ServiceLocator;

class FooBar extends Command
{

    public static $defaultName = 'foobar';
    private $locator;

    public function __construct(ServiceLocator $locator)
    {
        $this->locator = $locator;
        parent::__construct('hello');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $one = $this->locator->get('fooFile');
        $two = $this->locator->get('barFile');

        $output->writeln($one->process('hello'));
        $output->writeln($two->process('something something'));

        return 0;
    }


}
